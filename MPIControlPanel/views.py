from django.shortcuts import render
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required, user_passes_test
from MPIControlPanel.models import Workstation

# Create your views here.

@login_required
@user_passes_test(lambda u: u.groups.filter(name='user').count() == 0, login_url='/')
def renderer(request):
	x = 150; #500px to 100%
	return render_to_response("renderer.html", {'full_name': request.user.username, 'percentageDone': x})
	
