from django.db import models

# Create your models here.

class Workstation(models.Model):
	#def __init__(self, _number, _name):
	#self.number = _number
	#self.workspaceName = _name	
	number = models.IntegerField()
	percentageDone = models.IntegerField()
	workspaceName = models.CharField(max_length="20")
	ipv4_address = models.IPAddressField(verbose_name=('ipv4 address'), blank=True, null=True, unique=True, default=None)
		
	
	def __unicode__(self):
		return self.number
		
class Final(models.Model):
	render = False; #jeżeli wyrenderowana scena jest gotowa do wyświetlenia to True
	