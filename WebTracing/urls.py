from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'WebTracing.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
	url(r'^$', 'WebTracing.views.login'),
	url(r'^accounts/auth/$', 'WebTracing.views.auth_view'),
	url(r'^accounts/logout/$', 'WebTracing.views.logout'),
	url(r'^welcome$', 'MPIControlPanel.views.renderer'),
	url(r'^accounts/invalid/$', 'WebTracing.views.invalid'),	
	url(r'^accounts/login/&', 'WebTracing.views.invalid'),
)
